#include <iostream>
using namespace std;

int main()
{
    // Déclaration et instanciation de variable int
	int a, b, r;
	int a1, b1;
    
    // Affichage du nom du programme
	cout<<"Algorithme d Euclide\n"<<endl;

    // Demander de saisir le 1er nombre et récupérer sa valeur dans la variable a
	cout<<"Saisir le premier nombre : "<<endl;
	cin>>a;
    
    // La variable a1 prend la valeur de la variable a
	a1 = a;

    // Demander de saisir le 2ème nombre et récupérer sa valeur dans la variable b
	cout<<"\nSaisir le deuxieme nombre : \n"<<endl;
	cin>>b;

    // La variable b1 prend la valeur de la variable b
	b1 = b;

    // Rappel de l'objectif et des nombres utilisés
	cout<<"On cherche donc le PGCD des nombres "<<a<<" et "<<b<<"."<<endl;

    // La variable r prend la valeur du modulo de la division de a par b
	r = a % b;

    // Modifier les variables r, a et b jusqu'à ce que r = 0
	while(r != 0)
	{
		r = a % b;
		a = b;
		b = r;
	}

    // Affichage du PGCD final avec les nombres choisis au départ
	cout<<"\n\nLe plus petit PGCD des nombres "<<a1<<" et "<<b1<<" est : "<<a<<endl;

	return 0
	
	// Modification du fichier avec ajout d'un commentaire
}